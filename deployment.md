# 部署说明

## 小程序

### 如何下载代码
```
git clone git@gitee.com:lb002/tcb-hackthon-best-geek.git
```

### 如何将代码导入到开发者工具

下载[微信开发者工具](https://developers.weixin.qq.com/miniprogram/dev/devtools/download.html)，点击菜单“项目 > 导入项目”，按照提示导入小程序代码。具体请参考[微信小程序开发文档](https://developers.weixin.qq.com/miniprogram/dev/framework/)

### 哪些参数需要修改

代码导入过程中，可以修改appid，或者继续使用项目代码中的appid。如果更换了appid，请登录微信小程序官方后台修改request合法域名和uploadFile合法域名为你自己的服务端域名。

### 哪些云函数需要部署

如果更换了appid，请创建名为tcp-best-geek的云函数环境，并上传项目中mask-ui/cloudfunction目录自带的云函数。具体操作请参考[云开发文档](https://cloud.tencent.com/document/product/876/19360)。

### 涉及到的外部服务

需要部署后端API、管理后台、AI图像识别代码。

### 云数据库中需要创建哪些数据

暂未使用云数据库。

### 云存储中需要上传哪些文件

需要上传[小程序操作视频教程](https://7463-tcb-best-geek-2kr5n-1301287021.tcb.qcloud.la/1581041635901600.mp4?sign=e0f7a3f4fc71a9aae2df69d7c8c2a2fa&t=1581693396)。

## 后端API

请参考[Lumen开发文档](https://lumen.laravel.com/docs/5.5/)。大致流程为执行```composer install -vvv```，修改数据库、redis等配置，给相应目录添加nginx用户的写权限。

## 管理后台

请参考[Yii2开发文档](https://www.yiiframework.com/doc/guide/2.0/zh-cn)。大致流程为执行```composer install -vvv```，参考示例配置文件(config/*-example.php)，新增env配置文件(config/*-env.php)，修改数据库、后台密码(config/params-env.php中)等配置，给相应目录添加nginx用户的写权限。

## AI图像识别

1. 安装php的imagick扩展
2. 安装composer依赖，例如```composer install -vvv```
3. 在recognitionapi/storage目录下执行如下命令
```
ln -s ../../mask-verifier/model/valve.model ./valve.model
ln -s ../../mask-verifier/model/print.model ./print.model
```