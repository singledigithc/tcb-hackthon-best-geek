<?php

$config = [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
];

$envConfig = require __DIR__ . '/params-env.php';

return array_merge($config, $envConfig);
