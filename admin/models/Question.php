<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "question".
 *
 * @property int $id ID
 * @property string $title 题目名称
 * @property int $question_type 1单选，2多选，3填空
 * @property string $options 题目选项
 * @property string $ans_config 答案配置
 * @property string $example_pic 示例图
 * @property string $created_at 创建时间
 * @property string $updated_at 修改时间
 */
class Question extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'question';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['question_type'], 'integer'],
            [['options', 'ans_config', 'example_pic'], 'required'],
            [['options', 'ans_config', 'example_pic'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => '题目名称',
            'question_type' => '1单选，2多选，3填空',
            'options' => '题目选项',
            'ans_config' => '答案配置',
            'example_pic' => '示例图',
            'created_at' => '创建时间',
            'updated_at' => '修改时间',
        ];
    }

    /**
     * {@inheritdoc}
     * @return QuestionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new QuestionQuery(get_called_class());
    }
}
