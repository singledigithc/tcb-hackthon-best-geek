<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post_category".
 *
 * @property int $id ID
 * @property string $name 分类名称
 * @property int $parent_category_id 父类
 * @property string $icon 分类图标
 * @property int $is_default 是否默认显示分类
 * @property string $created_at 创建时间
 * @property string $updated_at 修改时间
 */
class PostCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_category_id', 'is_default'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'icon'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => '分类名称',
            'parent_category_id' => '父类',
            'icon' => '分类图标',
            'is_default' => '是否默认显示分类',
            'created_at' => '创建时间',
            'updated_at' => '修改时间',
        ];
    }

    /**
     * {@inheritdoc}
     * @return PostCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PostCategoryQuery(get_called_class());
    }
}
