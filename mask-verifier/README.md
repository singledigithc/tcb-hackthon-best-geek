# mask-verifier

## 激光刻蚀(先清除data、model、processed、samples/print/train目录下的数据)

```
php sample_generator.php 1
php image2data.php 1
php train.php 1

Collecting samples...
Finished at: 0.063060998916626s
Processing samples...
Finished at: 0.088917016983032s
Network init...
Finished at: 0.089891910552979s
Training...
Finished at: 0.34120488166809s
Evaluating...
Score: 0.66666666666667
Finished at: 0.36074900627136s
Saving model...
Finished at: 0.44908094406128s
```

## 呼吸阀(先清除data、model、processed、samples/valve/train目录下的数据)

```
php sample_generator.php 2
php image2data.php 2
php train.php 2

Collecting samples...
Finished at: 0.11211705207825s
Processing samples...
Finished at: 0.1632239818573s
Network init...
Finished at: 0.16421413421631s
Training...
Finished at: 0.63243103027344s
Evaluating...
Score: 0.75
Finished at: 0.65664005279541s
Saving model...
Finished at: 0.81034898757935s
```
