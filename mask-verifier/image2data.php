<?php

require_once __DIR__ . '/vendor/autoload.php';

$flag = isset($argv[1]) ? $argv[1] : 1;
if ($flag == 1) {
    $type = 'print';
} else {
    $type = 'valve';
}

function process($imagePath, $processedImagePath)
{
    $imagick = new Imagick($imagePath);
    $imagick->scaleImage(28, 28);


    $pixels = [];
    $pixelsIterator = $imagick->getPixelIterator();
    foreach ($pixelsIterator as $row => $pixelList) {
        foreach ($pixelList as $col => $pixel) {
            $blueValue = $pixel->getColorValue(Imagick::COLOR_BLUE);
            $greenValue = $pixel->getColorValue(Imagick::COLOR_GREEN);
            $redValue = $pixel->getColorValue(Imagick::COLOR_RED);
            $pixelVal = $blueValue + $greenValue + $redValue;
            if ($pixelVal < 3) {
                $pixels[] = $blueValue + $greenValue + $redValue;
            }
        }
    }

    $avgPixel = array_sum($pixels) / count($pixels);
    $filteredPixels = [];

    $pixelsIterator = $imagick->getPixelIterator();
    foreach ($pixelsIterator as $row => $pixelList) {
        foreach ($pixelList as $col => $pixel) {
            $blueValue = $pixel->getColorValue(Imagick::COLOR_BLUE);
            $greenValue = $pixel->getColorValue(Imagick::COLOR_GREEN);
            $redValue = $pixel->getColorValue(Imagick::COLOR_RED);
            $pixelVal = $blueValue + $greenValue + $redValue;
            if ($pixelVal < 3) {
                if ($pixelVal >= $avgPixel) {
                    $pixel->setColorValue(Imagick::COLOR_BLUE, 1);
                    $pixel->setColorValue(Imagick::COLOR_GREEN, 1);
                    $pixel->setColorValue(Imagick::COLOR_RED, 1);
                    $pixelVal = 3;
                }
            }
            $filteredPixels[] = $pixelVal;
        }
        $pixelsIterator->syncIterator();
    }

    $imagick->writeImage($processedImagePath);

    $imagick->destroy();

    return $filteredPixels;
}

$dataDir = __DIR__ . '/data/' . $type;
if (!is_dir($dataDir)) {
    mkdir($dataDir, 0777, true);
}

$processedTrainDir = __DIR__ . '/samples/' . $type . '/processed/train/';
if (!is_dir($processedTrainDir)) {
    mkdir($processedTrainDir, 0777, true);
}

$processedTestDir = __DIR__ . '/samples/' . $type . '/processed/test/';
if (!is_dir($processedTestDir)) {
    mkdir($processedTestDir, 0777, true);
}

$csv = fopen($dataDir . '/train.csv', 'wb');

$dir = opendir(__DIR__ . '/samples/' . $type . '/train');
while ($file = readdir($dir)) {
    if ((!in_array($file, ['.', '..'])) && (!(strpos($file, '.') === 0))) {
        $fileNameParts = explode('.', $file);
        $imageNameParts = explode('_', $fileNameParts[0]);
        $label = $imageNameParts[1];

        $samples = process(
            __DIR__ . '/samples/' . $type . '/train/' . $file,
            $processedTrainDir . $file
        );
        $samples[] = $label;

        fputcsv($csv, $samples);
    }
}
closedir($dir);

fclose($csv);


$csv = fopen($dataDir . '/test.csv', 'wb');

$dir = opendir(__DIR__ . '/samples/' . $type . '/test');
while ($file = readdir($dir)) {
    if ((!in_array($file, ['.', '..'])) && (!(strpos($file, '.') === 0))) {
        $fileNameParts = explode('.', $file);
        $imageNameParts = explode('_', $fileNameParts[0]);
        $label = $imageNameParts[1];

        $samples = process(
            __DIR__ . '/samples/' . $type . '/test/' . $file,
            $processedTestDir . $file
        );
        $samples[] = $label;

        fputcsv($csv, $samples);
    }
}
closedir($dir);

fclose($csv);
