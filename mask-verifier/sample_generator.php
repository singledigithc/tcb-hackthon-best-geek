<?php

require_once __DIR__ . '/vendor/autoload.php';

$flag = isset($argv[1]) ? $argv[1] : 1;
if ($flag == 1) {
    $type = 'print';
} else {
    $type = 'valve';
}

function processTrain($imagePath, $label, $degree, $type)
{
    $imagick = new Imagick($imagePath);
    $imagick->rotateImage('#ffffff', $degree);
    $trainDir = __DIR__ . '/samples/' . $type . '/train/';
    if (!is_dir($trainDir)) {
        mkdir($trainDir, 0777, true);
    }
    $newImagePath = $trainDir . ((string)intval(microtime(true) * 1000)) .
        '_' . $label . '.jpeg';
    $imagick->writeImage($newImagePath);
    $imagick->destroy();
}

$dir = opendir(__DIR__ . '/samples/' . $type . '/seed');
while ($file = readdir($dir)) {
    if ((!in_array($file, ['.', '..'])) && (!(strpos($file, '.') === 0))) {
        $fileNameParts = explode('.', $file);
        $imageNameParts = explode('_', $fileNameParts[0]);
        $label = $imageNameParts[1];
        for ($i = -45; $i <= 45; ++$i) {
            processTrain(__DIR__ . '/samples/' . $type . '/seed/' . $file, $label, $i, $type);
        }
    }
}
closedir($dir);
