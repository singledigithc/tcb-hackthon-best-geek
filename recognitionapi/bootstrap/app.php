<?php

require_once __DIR__.'/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(__DIR__.'/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../')
);

//日志文件输出格式
$app->configureMonologUsing(function(Monolog\Logger $monolog) use ($app) {
    return $monolog->pushHandler(
        new \Monolog\Handler\RotatingFileHandler($app->storagePath().'/logs/lumen.log')
    );
});

//加载自定义的配置
$app->configure('app');
//数据库配置对应的配置文件
$app->configure('database');
//api接口的参数校验对应的配置文件
$app->configure('rules');
//api接口的错误码对应的配置文件
$app->configure('errcode');
//缓存的配置文件
$app->configure('redis_key');

$app->withFacades();

$app->withEloquent();

//加载自定义的函数,不同于laravel中的直接在composer的autoload中进行加载
require_once __DIR__.'/../app/Helper/CommonHelper.php';

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/


$app->middleware([
    \App\Http\Middleware\Cors::class,
]);

$app->routeMiddleware([
    'logdb.send'=> App\Http\Middleware\LogdbMiddleware::class,
]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

// Common 业务服务容器
$app->register(App\Providers\AppServiceProvider::class);
// 其他数据库的服务容器
$app->register(App\Providers\OtherServiceProvider::class);
//Redis缓存的服务容器
$app->register(Illuminate\Redis\RedisServiceProvider::class);
//sentry服务
$app->register(Sentry\SentryLaravel\SentryLumenServiceProvider::class);

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$app->router->group([ 'namespace' => 'App\Http\Controllers', ], function ($router) {
    require __DIR__.'/../routes/web.php';
});
//api接口
$app->router->group([ 'namespace' => 'App\Http\Api\V1\Controllers', 'prefix' => '/v1' ], function ($router) {
    require __DIR__.'/../routes/api_v1.php';
});

//rpc接口，第三方调用
$app->router->group([ 'namespace' => 'App\Http\Rpc\V1\Controllers', 'prefix' => '/rpc/v1' ], function ($router) {
    require __DIR__.'/../routes/rpc_v1.php';
});

return $app;
