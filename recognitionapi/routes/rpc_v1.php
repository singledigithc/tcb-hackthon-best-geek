<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Laravel\Lumen\Routing\Router;

/** @var Router $router */
$router->get('/', function () use ($router) {
    return 'rpc-v1';
});

//有赞系统推送积分的接口
$router->group(['prefix' => '/yz'], function () use ($router) {
    //有赞消息推送获取的接口
    $router->post('/points-message', 'YzController@pointsMessage');

});
