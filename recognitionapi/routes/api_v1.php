<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2019/7/12
 * Time: 20:02
 */

use Laravel\Lumen\Routing\Router;

/** @var Router $router */
$router->get('/', function () use ($router) {
    return 'recognition-v1';
});

$router->group(['middleware' => [ 'logdb.send' ]], function () use ($router) {
    $router->group([ 'prefix' => '/index' ], function () use ($router) {
        //获取题目信息
        $router->get('/question-info', 'IndexController@getQuestionInfo');
        //图片识别口罩
        $router->post('/recognition-img', 'IndexController@recognitionImg');
        //问卷识别口罩
        $router->post('/recognition-question', 'IndexController@recognitionQuestion');
        //识别未戴口罩人群
        $router->post('/mask-detect', 'IndexController@maskDetect');
    });

    //用户线索
    $router->group(['prefix' => '/clue'], function () use ($router) {
        //添加线索
        $router->post('/add', 'ClueController@addClue');
    });

    //资讯
    $router->group([ 'prefix' => '/post' ], function () use ($router) {
        //资讯列表
        $router->get('/list', 'PostController@getPostList');
        //文章内容
        $router->get('/detail', 'PostController@getPostDetail');

        //资讯分类
        $router->group(['prefix' => '/category'], function () use ($router) {
            //资讯分类列表
            $router->get('/list', 'PostController@getPostCategoryList');
        });
    });
});
