<?php

return [
    //用户模块
    'user' => [
        'authorization' => [
            'rules' => [
                'code' => 'required',
                'iv' => 'required',
                'encrypted_data' => 'required',
                'appid' => 'required',
            ],
            'messages' =>[
                'code.required' => 'user.code_invalid',
                'iv.required' => 'user.iv_invalid',
                'encrypted_data.required' => 'user.encrypted_data_invalid',
                'appid.required' => 'user.appid_invalid',
            ]
        ],
    ],
];