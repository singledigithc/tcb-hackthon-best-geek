<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2016/11/4
 * Time: 14:29
 *
 * 公共的参数错误吗以10001开始
 * 业务参数模块错误码分配详情
 */

return [
    'success' => [
        'code' => 200,
        'msg' => '成功'
    ],
    'fail' => [
        'code' => -1,
        'msg' => '失败'
    ],
    'error' => [
        'code' => 1,
        'msg' => '系统异常'
    ],

    // 参数错误
    'param' => [
        'not_null' => [
            'code' => 100001,
            'msg' => 'param 参数错误'
        ],
        'encode_error' => [
            'code' => 100002,
            'msg' => 'param 参数错误'
        ],
        'not_json' => [
            'code' => 100003,
            'msg' => 'param 参数错误'
        ],
        'validate_error' => [
            'code' => 100005,
            'msg' => 'data 参数校验错误'
        ]
    ],

    'user' => [
        'code_invalid' =>[
            'code' => 200001 ,
            'msg' => '参数错误'
        ],
        'encrypted_data_invalid' =>[
            'code' => 200002,
            'msg' => '参数错误'
        ],
        'iv_invalid' =>[
            'code' => 200003,
            'msg' => '参数错误'
        ],
        'appid_invalid' => [
            'code' => 200004,
            'msg' => '参数错误'
        ]
    ]
];