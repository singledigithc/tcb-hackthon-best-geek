<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2016/11/4
 * Time: 14:29
 */

return [
    'run_model' => env('RUN_MODEL', 'points::'),

    'cache_key' => [
        'file_md5' => 'file_md5:',
        //请求频繁
        'request_frequently' => 'request_frequently:',
    ]
];