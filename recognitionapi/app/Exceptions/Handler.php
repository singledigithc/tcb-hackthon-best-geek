<?php

namespace App\Exceptions;

use App\Http\Common\Helper\FormatHelper;
use App\Http\Common\Helper\LogHelper;
use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $e
     * @return void
     * @throws Exception
     */
    public function report(Exception $e)
    {
        if (app()->bound('sentry') && $this->shouldReport($e)) {
            if ($e->getCode() != -10000) {      //Exception -10000 不记录到sentry
                app('sentry')->captureException($e);
            }
        }

        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof UserException) {
            return FormatHelper::failCode($e->getCode(), $e->getMessage());
        } elseif ($e instanceof ParamException) {
            return FormatHelper::fail($e->getMessage());
        } else {
            //监控不存在的接口调用
            if ($e instanceof NotFoundHttpException) {
                LogHelper::warning('uri 不存在: ' . $request->path() . ' 调用IP: ' . getIp(), [$request->all()]);
            }

            if (env('APP_ENV') == 'production') {
                $data = [
                    'code' => $e->getCode(),
                    'trace' => $e->getTrace()
                ];
                LogHelper::warning($e->getMessage(), [
                    $e->getFile(),
                    $e->getLine(),
                    $data
                ]);
                return FormatHelper::fail();
            } else {
                return parent::render($request, $e);
            }
        }
    }
}
