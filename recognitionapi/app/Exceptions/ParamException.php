<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2019/7/16
 * Time: 16:44
 */

namespace App\Exceptions;

/**
 * 只有参数错误时使用
 * Class ParamException
 * @package App\Exceptions
 */
class ParamException extends \Exception
{
    public function __construct($codeKey)
    {
        parent::__construct($codeKey);
    }
}