<?php

namespace App\Http\Api\V1\Controllers;

use App\Http\Api\V1\Blls\PostBll;
use App\Http\Base\BaseController;
use App\Http\Common\Helper\FormatHelper;
use Illuminate\Http\Request;

class PostController extends BaseController
{
    /**
     * 获取资讯列表
     *
     * @param Request $request
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public function getPostList(Request $request)
    {
        $categoryId = intval($request->input('catid'));
        $limit = intval($request->input('limit'));
        $keyword = $request->input('keyword');
        return FormatHelper::success(
            PostBll::getPostList($categoryId, $limit, 'updated_at desc', $keyword)
        );
    }

    /**
     * 获取资讯文章内容
     *
     * @param Request $request
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public function getPostDetail(Request $request)
    {
        $postId = intval($request->input('postid'));
        return FormatHelper::success(PostBll::getPostDetail($postId));
    }

    /**
     * 获取资讯分类列表
     *
     * @param Request $request
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public function getPostCategoryList(Request $request)
    {
        $limit = intval($request->input('limit'));
        $isDefault = intval($request->input('isdefault'));
        return FormatHelper::success(PostBll::getPostCategoryList($limit, $isDefault, 'id asc'));
    }
}
