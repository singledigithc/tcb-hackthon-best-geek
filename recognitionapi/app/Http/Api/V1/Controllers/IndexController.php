<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2020/2/3
 * Time: 19:07
 */

namespace App\Http\Api\V1\Controllers;

use App\Http\Api\V1\Blls\QuestionBll;
use App\Http\Api\V1\Blls\RecognitionBll;
use App\Http\Base\BaseController;
use App\Http\Common\Helper\FormatHelper;
use Illuminate\Http\Request;

class IndexController extends BaseController
{
    /**
     * 获取题目信息
     * @param Request $request
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public function getQuestionInfo(Request $request)
    {
        $param = $request->input();
        return FormatHelper::success(QuestionBll::getQuestion($param));
    }

    /**
     * 根据图片口罩信息识别
     * @param Request $request
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \App\Exceptions\UserException
     */
    public function recognitionImg(Request $request)
    {
        $sourceImage = $request->file('image');
        $param = $request->input();
        return FormatHelper::success(RecognitionBll::recognitionImg($sourceImage, $param));
    }

    /**
     * 根据问卷口罩信息识别
     * @param Request $request
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \App\Exceptions\UserException
     */
    public function recognitionQuestion(Request $request)
    {
        $param = $request->input();
        return FormatHelper::success(RecognitionBll::recognitionQuestion($param));
    }

    /**
     * 识别图片中未戴口罩的人
     * @param Request $request
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public function maskDetect(Request $request)
    {
        $image = $request->file('image');
        $imagePath = $image->getRealPath();
        if ($imagePath === false) {
            return FormatHelper::failCode(-1, '检测失败，请稍后重试', []);
        }

        $detectResult = RecognitionBll::maskDetect($imagePath, $image->getClientOriginalExtension());
        unlink($imagePath);
        if (!isset($detectResult[0]['base64'])) {
            return FormatHelper::failCode(-1, '检测失败，请稍后重试', $detectResult);
        }

        return FormatHelper::success($detectResult);
    }
}
