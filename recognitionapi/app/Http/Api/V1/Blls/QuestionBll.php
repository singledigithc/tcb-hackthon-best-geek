<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2020/2/3
 * Time: 19:08
 */

namespace App\Http\Api\V1\Blls;


use App\Http\Common\Facade\QuestionFacade;

class QuestionBll
{
    public static function getQuestion($param)
    {
        $list = QuestionFacade::getAll();
        $data = [];
        foreach ($list as $question) {
            $data[] = [
                'id' => $question->id,
                'title' => $question->title,
                'options' => json_decode($question->options, true),
                'example_pic' => json_decode($question->example_pic, true),
            ];
        }
        return $data;
    }
}