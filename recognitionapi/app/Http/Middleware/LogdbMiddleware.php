<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2018/11/29
 * Time: 17:39
 */

namespace App\Http\Middleware;

use App\Http\Common\Constant\HeaderConst;
use App\Http\Common\Helper\Th;
use Closure;

class LogdbMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $time = Th::millisecond();
        $response = $next($request);
        //if (env('APP_ENV') == 'production') {
        $businessData = $request->input() ?: [];
        $param = json_encode($businessData);
        $path = $request->path();   //访问的路径
        $uid = $request->header(HeaderConst::X_UUID);  //用户唯一ID
        $system = $request->header(HeaderConst::X_SYSTEM);     //用户操作系统
        $brand = $request->header(HeaderConst::X_BRAND);       //手机品牌
        $model = $request->header(HeaderConst::X_MODEL);       //手机型号
        $version = $request->header(HeaderConst::X_VERSION);   //微信版本
        $appVersion = $request->header(HeaderConst::X_APP_VERSION);    //应用程序版本
        $appSubject = $request->header(HeaderConst::X_APP_SUBJECT);    //应用程序主体
        $data = [
            'module' => env('APP_ENV') == 'production' ? 'points' : 'points_test',
            'uid' => $uid ? $uid : (isset($businessData['uid']) ? $businessData['uid'] : 0),
            'apiName' => $path,
            'requestMethod' => $request->method(),
            'paramJson' => substr($param, 0, 1000),
            'response' => '',
            'status' => 200,
            'errCode' => 0,
            'errMessage' => '',
            'userIp' => '',
            'serverIp' => '',
            'endTime' => 0,
            'spendTime' => 0,
            'system' => $system ? $system : '',
            'brand' => $brand ? $brand : '',
            'model' => $model ? $model : '',
            'version' => $version ? $version : '',
            'appVersion' => $appVersion ? $appVersion : '',
            'appSubject' => $appSubject ? $appSubject : '',
        ];

        $endTime = Th::millisecond();
        $data['response'] = strlen($response->content()) > 3000 ? substr($response->content(), 0, 3000) : $response->content();
        $data['status'] = intval($response->getStatusCode());
        $data['errCode'] = intval(formatArrValue($response->original, 'code', 200));
        $data['errMessage'] = formatArrValue($response->original, 'message', '');
        $ipArr = explode(',', getIp());
        $data['userIp'] = trim($ipArr[count($ipArr) - 1]);
        $data['serverIp'] = $_SERVER["SERVER_ADDR"] ? $_SERVER["SERVER_ADDR"] : '';
        $data['endTime'] = intval($endTime / 1000);
        $data['spendTime'] = intval($endTime - $time);

        $message = json_encode($data);
        $len = strlen($message);

        $sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
        @socket_sendto($sock, $message, $len, 0, '39.106.157.197', 1217);
        socket_close($sock);
        //}

        return $response;
    }
}