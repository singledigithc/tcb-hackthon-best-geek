<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2020/02/04
 * Time: 15:32
 */

namespace App\Http\Common\Facade;

use App\Http\Common\Model\Recognition;
use App\Http\Common\Repository\RecognitionRepository;
use Illuminate\Support\Facades\Facade;

/**
 * @see RecognitionRepository
 *
 * Class RecognitionFacade
 * @package App\Http\Common\Facade
 *
 * @method static Recognition[] getAll()
 * @method static Recognition getOneById(int $id)
 * @method static mixed batchInsertData(array $insetDataList)
 * @method static mixed batchUpdate(array $updateDataList)
 * @method static Recognition save(array $recognitionData)
 * @method static int update(array $recognitionData)
 * @method static mixed deleted(int $id)
 */
class RecognitionFacade extends Facade{
    protected static function getFacadeAccessor(){
        return 'RecognitionRepository';
    }
}