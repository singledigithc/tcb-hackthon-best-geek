<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2020/02/04
 * Time: 15:32
 */

namespace App\Http\Common\Facade;

use App\Http\Common\Model\Question;
use App\Http\Common\Repository\QuestionRepository;
use Illuminate\Support\Facades\Facade;

/**
 * @see QuestionRepository
 *
 * Class QuestionFacade
 * @package App\Http\Common\Facade
 *
 * @method static Question[] getAll()
 * @method static Question getOneById(int $id)
 * @method static mixed batchInsertData(array $insetDataList)
 * @method static mixed batchUpdate(array $updateDataList)
 * @method static Question save(array $questionData)
 * @method static int update(array $questionData)
 * @method static mixed deleted(int $id)
 */
class QuestionFacade extends Facade{
    protected static function getFacadeAccessor(){
        return 'QuestionRepository';
    }
}