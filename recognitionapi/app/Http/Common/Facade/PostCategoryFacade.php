<?php

namespace App\Http\Common\Facade;

use App\Http\Common\Model\PostCategory;
use App\Http\Common\Repository\PostCategoryRepository;
use Illuminate\Support\Facades\Facade;

/**
 * @see PostCategoryRepository
 *
 * Class PostCategoryFacade
 * @package App\Http\Common\Facade
 *
 * @method static PostCategory[] getAll()
 * @method static PostCategory getOneById(int $id)
 * @method static PostCategory[] getByIds(array $ids)
 * @method static mixed batchInsertData(array $insetDataList)
 * @method static mixed batchUpdate(array $updateDataList)
 * @method static PostCategory save(array $postCategoryData)
 * @method static int update(array $postCategoryData)
 * @method static mixed deleted(int $id)
 * @method static PostCategory[] getByCondition(int $limit, int $isDefault, $order)
 */
class PostCategoryFacade extends Facade{
    protected static function getFacadeAccessor()
    {
        return 'PostCategoryRepository';
    }
}
