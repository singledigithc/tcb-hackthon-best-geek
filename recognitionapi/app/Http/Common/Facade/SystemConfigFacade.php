<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2020/02/04
 * Time: 15:32
 */

namespace App\Http\Common\Facade;

use App\Http\Common\Model\SystemConfig;
use App\Http\Common\Repository\SystemConfigRepository;
use Illuminate\Support\Facades\Facade;

/**
 * @see SystemConfigRepository
 *
 * Class SystemConfigFacade
 * @package App\Http\Common\Facade
 *
 * @method static SystemConfig[] getAll()
 * @method static SystemConfig getOneById(int $id)
 * @method static mixed batchInsertData(array $insetDataList)
 * @method static mixed batchUpdate(array $updateDataList)
 * @method static SystemConfig save(array $systemConfigData)
 * @method static int update(array $systemConfigData)
 * @method static mixed deleted(int $id)
 */
class SystemConfigFacade extends Facade{
    protected static function getFacadeAccessor(){
        return 'SystemConfigRepository';
    }
}