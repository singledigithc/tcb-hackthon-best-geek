<?php

namespace App\Http\Common\Facade;

use App\Http\Common\Model\Post;
use App\Http\Common\Repository\PostRepository;
use Illuminate\Support\Facades\Facade;

/**
 * @see PostRepository
 *
 * Class PostFacade
 * @package App\Http\Common\Facade
 *
 * @method static Post[] getAll()
 * @method static Post getOneById(int $id)
 * @method static mixed batchInsertData(array $insetDataList)
 * @method static mixed batchUpdate(array $updateDataList)
 * @method static Post save(array $postData)
 * @method static int update(array $postData)
 * @method static mixed deleted(int $id)
 * @method static Post[] getByCondition($categoryId, int $limit, $order, $keyword = null)
 */
class PostFacade extends Facade{
    protected static function getFacadeAccessor()
    {
        return 'PostRepository';
    }
}
