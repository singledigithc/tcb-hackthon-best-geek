<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2020/02/05
 * Time: 09:39
 */

namespace App\Http\Common\Facade;

use App\Http\Common\Model\Attachment;
use App\Http\Common\Repository\AttachmentRepository;
use Illuminate\Support\Facades\Facade;

/**
 * @see AttachmentRepository
 *
 * Class AttachmentFacade
 * @package App\Http\Common\Facade
 *
 * @method static Attachment[] getAll()
 * @method static Attachment getOneById(int $id)
 * @method static mixed batchInsertData(array $insetDataList)
 * @method static mixed batchUpdate(array $updateDataList)
 * @method static Attachment save(array $attachmentData)
 * @method static int update(array $attachmentData)
 * @method static mixed deleted(int $id)
 */
class AttachmentFacade extends Facade{
    protected static function getFacadeAccessor(){
        return 'AttachmentRepository';
    }
}