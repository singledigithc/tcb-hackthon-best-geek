<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2018/12/4
 * Time: 14:07
 */

namespace App\Http\Common\Helper;

class CurlHelper
{
    /**
     * CURL_POST
     * @param $url
     * @param array|string $params
     * @param string $format
     * @param array $optionExtends
     * @param int $timeout
     * @return mixed
     */
    public static function post($url, $params, $format = 'json', $optionExtends = [], $timeout = 120) {
        $startAt = microtime(true);

        $ch = curl_init($url);
        if (is_array($params)) {
            $options = array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_TIMEOUT => $timeout,
                CURLOPT_FOLLOWLOCATION => 1,
                CURLOPT_POSTFIELDS => $params,
                CURLOPT_SSL_VERIFYPEER => false, //不进行ssl证书验证
            );
            if ($format == 'json') {
                $options[CURLOPT_HTTPHEADER] = [
                    'Content-Type: application/json'
                ];
            }
        } else {
            $options = array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_TIMEOUT => $timeout,
                CURLOPT_FOLLOWLOCATION => 1,
                CURLOPT_POSTFIELDS => $params,
                CURLOPT_SSL_VERIFYPEER => false, //不进行ssl证书验证
            );
            if ($format == 'json') {
                $options[CURLOPT_HTTPHEADER] = [
                    'Content-Type: application/json'
                ];
            }
            //允许自定义添加修改配置
            if ($optionExtends) {
                $options[CURLOPT_HTTPHEADER] = array_merge($options[CURLOPT_HTTPHEADER], $optionExtends);
            }
        }

        curl_setopt_array($ch, $options);
        $result = curl_exec($ch);
        $info = curl_getinfo($ch);
        $errResponse = '';
        if ($info['http_code'] != 200) {
            $errResponse = $result;
            $result = false;
        }

        if ($result === false) {
            LogHelper::warning('Curl请求失败：' . $url . '|' . curl_error($ch) . '|' . $errResponse, [$errResponse]);
        }
        curl_close($ch);

        $finishAt = microtime(true);
        self::logSlowQuery($url, $params, $result, $startAt, $finishAt);

        return $result;
    }

    /**
     * CURL_GET
     * @param $url
     * @param array $params
     * @param array $extendOptions
     * @return mixed
     */
    public static function get($url, $params = [], $extendOptions = []) {
        $startAt = microtime(true);

        $query = http_build_query($params);
        $ch = curl_init($url . '?' . $query);
        $options = array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_TIMEOUT => 120,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_SSL_VERIFYPEER => false, //不进行ssl证书验证
        );

        if ($extendOptions) {
            foreach ($extendOptions as $key => $value) {
                $options[$key] = $value;
            }
        }
        curl_setopt_array($ch, $options);
        $result = curl_exec($ch);
        if ($result == false) {
            LogHelper::warning('Curl请求失败：' . $url . '?' . $query, [ $url, curl_error($ch), $query, $result]);
        }
        curl_close($ch);

        $finishAt = microtime(true);
        self::logSlowQuery($url, $params, $result, $startAt, $finishAt);

        return $result;
    }

    /**
     * 记录接口查询耗时
     * @param $url
     * @param $params
     * @param $response
     * @param $startAt
     * @param $finishAt
     */
    private static function logSlowQuery($url, $params, $response, $startAt, $finishAt) {
        $consume = $finishAt - $startAt;
        LogHelper::info('curl请求耗时:' . $url . ' params: ' . json_encode($params) . ' response: ' .
            json_encode($response) . ' spend_time: ' . $consume);
    }
}