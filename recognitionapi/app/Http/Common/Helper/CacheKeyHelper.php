<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2019/7/22
 * Time: 13:42
 */

namespace App\Http\Common\Helper;


class CacheKeyHelper
{
    /**
     * 获取请求频繁key
     * @param mixed ...$args
     * @return string
     */
    public static function getRequestFrequentlyKey(...$args)
    {
        return getCacheKey('redis_key.cache_key.request_frequently') . implode('_', $args);
    }
}