<?php
/**
 * Created by PhpStorm.
 * User: Auser
 * Date: 2017/7/16
 * Time: 20:47
 */

namespace App\Http\Common\Helper;

/**
 * 专门计算时间相关的Helper函数
 *
 * Th 代表 Time Helper，为写代码方便，用了缩写，因为用得多，所以短一点
 * Class Th
 * @package App\Http\Helper
 */
class Th
{
    const TIMESTAMP_MINUTE = 60;    //分钟的秒数
    const TIMESTAMP_HOUR = 3600;    //小时的秒数
    const TIMESTAMP_DAY = 86400;    //天的秒数
    const TIMESTAMP_MONTH = 2592000;    //月的秒数
    const TIMESTAMP_YEAR = 31536000;    //年的秒数

    /**
     * 根据相差的天数获取所有连续的时间段
     * @param $diffDay
     * @param string $dateFormat
     * @return array
     */
    public static function getContinuesDayDiffDay($diffDay, $dateFormat = 'Y-m-d')
    {
        $today = date('Y-m-d');
        $timeLabel = [];
        for ($i = 1; $i <= $diffDay; $i++) {
            $diff = $diffDay - $i;
            $mday = date($dateFormat, strtotime("$today -$diff day"));
            array_push($timeLabel, $mday);
        }

        //转化查询条件
        $startDay = str_replace('.', '-', $timeLabel[0]);
        $endDay = str_replace('.', '-', $timeLabel[$diffDay - 1]);
        $startTime = strtotime($startDay . " 00:00:00");
        $endTime = strtotime($endDay . " 23:59:59");
        return [
            'start_time' => $startTime,
            'end_time' => $endTime,
            'time_label' => $timeLabel,
        ];
    }

    /**
     * 根据开始和结束时间获取所有连续的时间段
     * @param string $startDay 开始日期 格式：Y-m-d
     * @param string $endDay 开始日期 格式：Y-m-d
     * @param string $dateFormat
     * @return array
     */
    public static function getContinuesDayByRange($startDay, $endDay, $dateFormat = 'Y-m-d')
    {
        $timeLabel = [];
        if (strtotime($startDay) > strtotime($endDay)) {
            $tmp = $startDay;
            $endDay = $tmp;
            $startDay = $endDay;
        }
        if ($startDay == $endDay) {
            array_push($timeLabel, $startDay);

            $startTime = strtotime($startDay . " 00:00:00");
            $endTime = strtotime($endDay . " 23:59:59");
            $timeLabel = [
                'start_time' => $startTime,
                'end_time' => $endTime,
                'time_label' => $timeLabel,
            ];
            return $timeLabel;
        }

        $targetDay = $startDay;
        while ($targetDay != $endDay) {
            array_push($timeLabel, $targetDay);
            $targetDay = date($dateFormat, strtotime("$targetDay +1 day"));
        }

        array_push($timeLabel, $endDay);

        //增加
        $startTime = strtotime($startDay . " 00:00:00");
        $endTime = strtotime($endDay . " 23:59:59");
        $timeLabel = [
            'start_time' => $startTime,
            'end_time' => $endTime,
            'time_label' => $timeLabel,
        ];
        return $timeLabel;
    }

    /**
     * 根据日期获取本月的开始时间和结束时间
     * @param $date Y-m    2017-10
     * @return array
     */
    public static function getMonthDaysByDate($date)
    {
        $data = [];
        $timestamp = strtotime($date);
        $data['start_time'] = date('Y-m-01 00:00:00', $timestamp);
        $mdays = date('t', $timestamp);
        $data['end_time'] = date('Y-m-' . $mdays . ' 23:59:59', $timestamp);
        return $data;
    }

    /**
     * 获取两个月份之间连续的月份
     * @param $start
     * @param $end
     * @return array
     */
    public static function prDates($start, $end)
    { // 两个日期之间的所有日期
        $time_start = strtotime($start); // 自动为00:00:00 时分秒 两个时间之间的年和月份
        $time_end = strtotime($end);
        $monarr[] = $start; // 当前月;
        while (($time_start = strtotime('+1 month', $time_start)) <= $time_end) {
            array_push($monarr, date('Y-m', $time_start));// 取得递增月
        }
        return $monarr;
    }

    /**
     * 获取星期几
     * @param $date
     * @return int
     */
    public static function getWeekDay($date)
    {
        //强制转换日期格式
        $dateStr = date('Y-m-d', strtotime($date));
        //封装成数组
        $arr = explode("-", $dateStr);
        //参数赋值
        //年
        $year = $arr[0];

        //月，输出2位整型，不够2位右对齐
        $month = sprintf('%02d', $arr[1]);
        //日，输出2位整型，不够2位右对齐
        $day = sprintf('%02d', $arr[2]);

        //时分秒默认赋值为0；
        $hour = $minute = $second = 0;

        //转换成时间戳
        $strap = mktime($hour, $minute, $second, $month, $day, $year);

        //获取数字型星期几
        $numberWk = date("w", $strap);

        //自定义星期数组
        $weekArr = array(7, 1, 2, 3, 4, 5, 6);

        //获取数字对应的星期
        return $weekArr[$numberWk];
    }

    /**
     * 获取指定日期前后相同时间天数的范围时间
     * @param int $dayDiff
     * @param string $day
     * @param string $dateFormat
     * @return array
     */
    public static function getPointDaySameRangeContinuesTime($dayDiff, $day = "", $dateFormat = "Y-m-d")
    {
        $day = $day ? $day : date($dateFormat);
        $startTime = date($dateFormat, strtotime("$day -$dayDiff day"));
        $endTime = date($dateFormat, strtotime("$day +$dayDiff day"));
        $result = self::getContinuesDayByRange($startTime, $endTime, $dateFormat);
        return $result;
    }

    /**
     * 获取两个日期之间相差的天数
     * @param string $day1 第一个日期，格式为Y-m-d
     * @param string $day2 第二个日期，格式为Y-m-d
     * @return integer
     */
    public static function getDiffBetweenTwoDays($day1, $day2)
    {
        $second1 = strtotime($day1);
        $second2 = strtotime($day2);
        if ($second1 < $second2) {
            $tmp = $second2;
            $second2 = $second1;
            $second1 = $tmp;
        }
        return ($second1 - $second2) / 86400;
    }

    /**
     * 根据日期和相差的天数获取结束的天数
     * @param $day
     * @param $diffDay
     * @param bool $isBefore
     * @return false|string
     */
    public static function getEndDayByDayAndDiff($day, $diffDay, $isBefore = false)
    {
        $operator = $isBefore ? "-" : "+";
        $endDay = date('Y-m-d', strtotime("$day $operator $diffDay day"));
        return $endDay;
    }

    /**
     * 根据时间戳返回日期型时间戳
     * @param $time
     * @return int
     */
    public static function dateTime($time)
    {
        return strtotime(date('Y-m-d', $time));
    }

    /**
     * 判断两个时间是否同一天
     * @param string $date1 Y-m-d
     * @param string $date2 Y-m-d
     * @return bool
     */
    public static function isSameDay($date1, $date2)
    {
        $day1 = self::dateTime(strtotime($date1));
        $day2 = self::dateTime(strtotime($date2));
        return $day1 == $day2;
    }


    /**
     * 转换秒钟为分钟
     * @param $seconds
     * @return string
     */
    public static function convertSecondToTime($seconds)
    {
        $reminded = strval($seconds % 60);
        $minute = strval(($seconds - $reminded) / 60);
        if (strlen($minute) < 2) {
            $minute = '0' . $minute;
        }
        if (strlen($reminded) < 2) {
            $reminded = '0' . $reminded;
        }
        $time = $minute . ":" . $reminded;
        return $time;
    }

    /**
     * 获取时间的毫秒数
     * @return float
     */
    public static function millisecond()
    {
        list($msec, $sec) = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
    }

    /**
     * 比较两个时间大小
     * @param $date1
     * @param $date2
     * @param $type 'hour','day'
     * @return int
     */
    public static function compareDate($date1, $date2, $type = 'second')
    {
        $time1 = strtotime($date1);
        $time2 = strtotime($date2);
        if ($type == 'day') {
            return round(($time1 - $time2) / 60 / 60 / 24);
        } elseif ($type == 'hour') {
            return round((($time1 - $time2) / 60 / 60));
        } elseif ($type == 'second') {
            return $time1 - $time2;
        }

        return false;
    }

    /**
     * 获取年月日格式
     * @param $time
     * @return false|string
     */
    public static function getTimeYmd($time)
    {
        return date('Y-m-d', $time);
    }

    public static function formatCompareCurrent($time)
    {
        $diff = time() - $time;
        if ($diff < static::TIMESTAMP_MINUTE) {
            return $diff . ' 秒前';
        } elseif ($diff < static::TIMESTAMP_HOUR) {
            return intval($diff / static::TIMESTAMP_MINUTE) . ' 分钟前';
        } elseif ($diff < static::TIMESTAMP_DAY) {
            return intval($diff / static::TIMESTAMP_HOUR) . '小时前';
        } elseif ($diff < static::TIMESTAMP_MONTH) {
            return intval($diff / static::TIMESTAMP_DAY) . ' 天前';
        } elseif ($diff < static::TIMESTAMP_YEAR) {
            return intval($diff / static::TIMESTAMP_MONTH) . ' 月前';
        }
        return intval($diff / static::TIMESTAMP_YEAR) . ' 年前';
    }
}