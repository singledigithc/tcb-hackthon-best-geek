<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2016/11/9
 * Time: 14:20
 */

namespace App\Http\Common\Helper;

use Illuminate\Support\Facades\Log;

class LogHelper {

    /**
     * 记录日志到storage里面的log下
     * @param $message
     * @param array $params
     */
    public static function info($message, $params = []) {
        $params = is_array($params) ? $params : [ $params ];
        Log::info($message, $params);
    }

    /**
     * 记录错误信息到sentry，并记录到storage里面的log下
     * @param $message
     * @param array $params
     * @param bool $tack
     */
    public static function warning($message, $params = [], $tack = true) {
        $params = is_array($params) ? $params : [ $params ];
        if (app()->bound('sentry')) {
            app('sentry')->captureMessage($message, $params, 'warning', $tack);
        }
        Log::warning($message, $params);
    }

    /**
     * 记录异常到sentry，并记录异常信息到storage里面的log下
     * @param $e
     */
    public static function exception($e) {
        if (app()->bound('sentry')) {
            app('sentry')->captureException($e);
        }
        Log::error($e->getMessage());
    }

    /**
     * 格式化消息
     * @param $message
     * @return string
     */
    public static function sprintfMsg($message) {
        return '[ ' . date('Y-m-d H:i:s') . ' ] ' . $message . PHP_EOL;
    }
}
