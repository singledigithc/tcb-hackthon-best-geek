<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2019/7/12
 * Time: 20:21
 */

namespace App\Http\Common\Helper;

class FormatHelper
{
    /**
     * 正常信息
     * @param array $data 数组
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public static function success($data = [])
    {
        $return = [
            'code'      => config('errcode.success.code'),
            'message'   => config('errcode.success.msg'),
            'data'      => $data,
        ];

        return response($return);
    }

    /**
     * 失败信息
     * @param string $codeKey
     * @param array $data
     * @param string $message
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public static function fail($codeKey = 'fail', $data = [], $message = null)
    {
        $return = [
            'code'      => config('errcode.' . $codeKey . '.code'),
            'message'   => $message ?: config('errcode.' . $codeKey . '.msg'),
            'data'      => $data,
        ];

        return response($return);
    }

    /**
     * 失败信息
     * @param string $code
     * @param array $data
     * @param string $message
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public static function failCode($code, $message = '系统异常，请稍后重试', $data = [])
    {
        $return = [
            'code'      => $code,
            'message'   => $message,
            'data'      => $data,
        ];

        return response($return);
    }
}