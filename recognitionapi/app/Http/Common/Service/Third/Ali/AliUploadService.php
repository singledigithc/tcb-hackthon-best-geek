<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2019/1/17
 * Time: 18:10
 */

namespace App\Http\Common\Service\Third\Ali;

use App\Exceptions\UserException;
use App\Http\Common\Helper\LogHelper;
use OSS\Core\OssException;
use OSS\OssClient;

class AliUploadService
{
    private $accessKeyId = 'LTAIuPuZCQnyKjo1';
    private $accessKeySecret = 'GJ8JUfOk60odEbFzUTn9CCcWY5te6I';
    private $bucket = 'yuanjy';
    private $endpoint = 'https://oss-cn-shanghai.aliyuncs.com';

    /**
     * AliUploadService constructor.
     */
    public function __construct(){

    }

    /**
     * 上传文件接口
     * @Author
     * @DateTime  2017-03-08
     * @param $dst
     * @param $src
     * @return string
     * @throws \OSS\Core\OssException
     */
    public function uploadFile($dst, $src){
        //获取对象
        $auth = new OssClient($this->accessKeyId, $this->accessKeySecret, $this->endpoint);
        $auth->setUseSSL(true);
        try {
            //上传图片
            $result  = $auth->uploadFile($this->bucket, $dst, $src);
            return $result['info']['url'];
        } catch (OssException $e) {
            throw $e;
        }
    }

    /**
     * 上传目录接口
     * @param $remoteDir
     * @param $localDir
     * @return string
     * @throws OssException
     */
    public function uploadDir($remoteDir, $localDir){
        //获取对象
        $auth = new OssClient($this->accessKeyId,$this->accessKeySecret,$this->endpoint);
        try {
            $auth->uploadDir($this->bucket, $remoteDir, $localDir);
            return true;
        } catch (OssException $e) {
            LogHelper::warning($e->getMessage());
            return $e->getMessage();
        }
    }
}