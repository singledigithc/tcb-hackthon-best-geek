<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2019/7/16
 * Time: 21:51
 */

namespace App\Http\Common\Service;

use App\Http\Common\Facade\SystemConfigFacade;

class BusinessService
{
    /**
     * demo
     */
    public static function getAccountDomain()
    {
        return SystemConfigFacade::getValue('account.domain');
    }
}