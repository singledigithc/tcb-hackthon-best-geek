<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2020/02/05
 * Time: 09:39
 */

namespace App\Http\Common\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Attachment
 * @package App\Http\Common\Model
 * 
 * @property int id
 * @property string url
 * @property string path
 * @property string created_at
 * @property string updated_at
 */
class Attachment extends Model
{
    /**
     * 表名
     */
    protected  $table = 'attachment';

    /**
     * 需要插入数据的列集合
     */
    protected $fillable = [
        'id',
        'url',
        'path',
        'created_at',
        'updated_at',
    ];

    /**
     * 是否启用时间戳，对应数据库表中的created_at 和 updated_at字段列
     */
    public $timestamps = false;
}