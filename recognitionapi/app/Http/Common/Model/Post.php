<?php

namespace App\Http\Common\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Post
 * @package App\Http\Common\Model
 * 
 * @property int id
 * @property string title
 * @property string content
 * @property int category_id
 * @property int author_id
 * @property string created_at
 * @property string updated_at
 */
class Post extends Model
{
    /**
     * 表名
     */
    protected  $table = 'post';

    /**
     * 需要插入数据的列集合
     */
    protected $fillable = [
        'id',
        'title',
        'content',
        'category_id',
        'author_id',
    ];

    /**
     * 是否启用时间戳，对应数据库表中的created_at 和 updated_at字段列
     */
    public $timestamps = false;
}
