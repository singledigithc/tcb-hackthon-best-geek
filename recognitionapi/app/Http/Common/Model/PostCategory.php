<?php

namespace App\Http\Common\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PostCategory
 * @package App\Http\Common\Model
 * 
 * @property int id
 * @property string name
 * @property string parent_category_id
 * @property string icon
 * @property int is_default
 * @property string created_at
 * @property string updated_at
 */
class PostCategory extends Model
{
    /**
     * 表名
     */
    protected  $table = 'post_category';

    /**
     * 需要插入数据的列集合
     */
    protected $fillable = [
        'id',
        'name',
        'parent_category_id',
    ];

    /**
     * 是否启用时间戳，对应数据库表中的created_at 和 updated_at字段列
     */
    public $timestamps = false;
}
