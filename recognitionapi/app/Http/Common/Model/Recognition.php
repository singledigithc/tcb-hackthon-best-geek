<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2020/02/04
 * Time: 15:32
 */

namespace App\Http\Common\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Recognition
 * @package App\Http\Common\Model
 * 
 * @property int id
 * @property string attachment_id
 * @property string answers
 * @property string reco_result
 * @property string final_result
 * @property string created_at
 * @property string updated_at
 */
class Recognition extends Model
{
    /**
     * 表名
     */
    protected  $table = 'recognition';

    /**
     * 需要插入数据的列集合
     */
    protected $fillable = [
        'id',
        'attachment_id',
        'answers',
        'reco_result',
        'final_result',
    ];

    /**
     * 是否启用时间戳，对应数据库表中的created_at 和 updated_at字段列
     */
    public $timestamps = false;
}