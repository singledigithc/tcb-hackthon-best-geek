<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2020/02/04
 * Time: 15:32
 */

namespace App\Http\Common\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SystemConfig
 * @package App\Http\Common\Model
 * 
 * @property int id
 * @property string name
 * @property string value
 * @property string description
 * @property string created_at
 * @property string updated_at
 */
class SystemConfig extends Model
{
    /**
     * 表名
     */
    protected  $table = 'system_config';

    /**
     * 需要插入数据的列集合
     */
    protected $fillable = [
        'id',
        'name',
        'value',
        'description',
        'created_at',
        'updated_at',
    ];

    /**
     * 是否启用时间戳，对应数据库表中的created_at 和 updated_at字段列
     */
    public $timestamps = false;
}