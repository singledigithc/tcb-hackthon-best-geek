<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2020/02/04
 * Time: 15:32
 */

namespace App\Http\Common\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Question
 * @package App\Http\Common\Model
 * 
 * @property int id
 * @property string title
 * @property int question_type
 * @property string options
 * @property string ans_config
 * @property string example_pic
 * @property string created_at
 * @property string updated_at
 */
class Question extends Model
{
    /**
     * 表名
     */
    protected  $table = 'question';

    /**
     * 需要插入数据的列集合
     */
    protected $fillable = [
        'id',
        'title',
        'question_type',
        'options',
        'ans_config',
        'example_pic',
    ];

    /**
     * 是否启用时间戳，对应数据库表中的created_at 和 updated_at字段列
     */
    public $timestamps = false;
}