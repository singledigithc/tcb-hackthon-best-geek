<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2020/02/04
 * Time: 15:32
 */

namespace App\Http\Common\Repository;

use App\Http\Base\BaseRepository;
use App\Http\Common\Model\Recognition;

class RecognitionRepository extends BaseRepository
{
    /**
     * 获取所有的数据
     * @return array
     */
    public function getAll()
    {
        return Recognition::all();
    }

    /**
     * 根据ID获取值
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|object|static|null
     */
    public function getOneById($id)
    {
        return Recognition::query()->where('id', $id)->first();
    }

    /**
     * 新增保存数据
     * @param array $recognitionData
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function handleSave(array $recognitionData)
    {
        return Recognition::query()->firstOrCreate($recognitionData);
    }

    /**
     * 修改数据
     * @param array $recognitionData
     * @return int
     */
    public function handleUpdate(array $recognitionData)
    {
        $id = $recognitionData['id'];
        unset($recognitionData['id']);
        return Recognition::query()->where('id', $id)->update($recognitionData);
    }

    /**
     * 删除数据
     * @param $id
     * @return mixed
     */
    public function handleDelete($id)
    {
        return Recognition::query()->where('id', $id)->delete();
    }

    /**
     * 获取表名
     * @return mixed|string
     */
    protected function getTableName()
    {
        return (new Recognition())->getTable();
    }
    
    /**
     * 获取缓存前缀信息
     * @return mixed|string
     */
    protected function getCachePrefix()
    {
        return 'recognition::';
    }
}