<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2020/02/05
 * Time: 09:39
 */

namespace App\Http\Common\Repository;

use App\Http\Base\BaseRepository;
use App\Http\Common\Model\Attachment;

class AttachmentRepository extends BaseRepository
{
    /**
     * 获取所有的数据
     * @return array
     */
    public function getAll()
    {
        return Attachment::all();
    }

    /**
     * 根据ID获取值
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|object|static|null
     */
    public function getOneById($id)
    {
        return Attachment::query()->where('id', $id)->first();
    }

    /**
     * 新增保存数据
     * @param array $attachmentData
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function handleSave(array $attachmentData)
    {
        return Attachment::query()->firstOrCreate($attachmentData);
    }

    /**
     * 修改数据
     * @param array $attachmentData
     * @return int
     */
    public function handleUpdate(array $attachmentData)
    {
        $id = $attachmentData['id'];
        unset($attachmentData['id']);
        return Attachment::query()->where('id', $id)->update($attachmentData);
    }

    /**
     * 删除数据
     * @param $id
     * @return mixed
     */
    public function handleDelete($id)
    {
        return Attachment::query()->where('id', $id)->delete();
    }

    /**
     * 获取表名
     * @return mixed|string
     */
    protected function getTableName()
    {
        return (new Attachment())->getTable();
    }
    
    /**
     * 获取缓存前缀信息
     * @return mixed|string
     */
    protected function getCachePrefix()
    {
        return 'attachment::';
    }
}