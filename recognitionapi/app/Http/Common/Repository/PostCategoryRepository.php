<?php

namespace App\Http\Common\Repository;

use App\Http\Base\BaseRepository;
use App\Http\Common\Model\PostCategory;

class PostCategoryRepository extends BaseRepository
{
    /**
     * 获取所有的数据
     * @return array
     */
    public function getAll()
    {
        return PostCategory::all();
    }

    public function getByCondition($limit, $isDefault, $order)
    {
        return PostCategory::query()
            ->where(['is_default' => $isDefault])
            ->orderByRaw($order)
            ->limit($limit)->get();
    }

    /**
     * 根据ID获取值
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|object|static|null
     */
    public function getOneById($id)
    {
        return PostCategory::query()->where('id', $id)->first();
    }

    /**
     * 根据id列表获取数据
     * @param array $ids
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getByIds(array $ids)
    {
        return PostCategory::query()->whereIn('id', $ids)->get();
    }

    /**
     * 新增保存数据
     * @param array $postData
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function handleSave(array $postData)
    {
        return PostCategory::query()->firstOrCreate($postData);
    }

    /**
     * 修改数据
     * @param array $postData
     * @return int
     */
    public function handleUpdate(array $postData)
    {
        $id = $postData['id'];
        unset($postData['id']);
        return PostCategory::query()->where('id', $id)->update($postData);
    }

    /**
     * 删除数据
     * @param $id
     * @return mixed
     */
    public function handleDelete($id)
    {
        return PostCategory::query()->where('id', $id)->delete();
    }

    /**
     * 获取表名
     * @return mixed|string
     */
    public function getTableName()
    {
        return (new PostCategory())->getTable();
    }
    
    /**
     * 获取缓存前缀信息
     * @return mixed|string
     */
    protected function getCachePrefix()
    {
        return 'post::category::';
    }
}
