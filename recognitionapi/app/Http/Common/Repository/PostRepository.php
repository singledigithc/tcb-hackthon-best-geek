<?php

namespace App\Http\Common\Repository;

use App\Http\Base\BaseRepository;
use App\Http\Common\Model\Post;
use Illuminate\Database\Eloquent\Builder;

class PostRepository extends BaseRepository
{
    /**
     * 获取所有的数据
     * @return array
     */
    public function getAll()
    {
        return Post::all();
    }

    public function getByCondition($categoryId, $limit, $order, $keyword = null)
    {
        $query = Post::query()->where([
            'category_id' => $categoryId
        ]);

        if (!is_null($keyword)) {
            $query->where(function (Builder $query) use ($keyword) {
                $query->where('title', 'LIKE', '%' . $keyword . '%')
                    ->orWhere('content', 'LIKE', '%' . $keyword . '%');
            });
        }

        return $query->orderByRaw($order)->limit($limit)->get();
    }

    /**
     * 根据ID获取值
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|object|static|null
     */
    public function getOneById($id)
    {
        return Post::query()->where('id', $id)->first();
    }

    /**
     * 新增保存数据
     * @param array $postData
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function handleSave(array $postData)
    {
        return Post::query()->firstOrCreate($postData);
    }

    /**
     * 修改数据
     * @param array $postData
     * @return int
     */
    public function handleUpdate(array $postData)
    {
        $id = $postData['id'];
        unset($postData['id']);
        return Post::query()->where('id', $id)->update($postData);
    }

    /**
     * 删除数据
     * @param $id
     * @return mixed
     */
    public function handleDelete($id)
    {
        return Post::query()->where('id', $id)->delete();
    }

    /**
     * 获取表名
     * @return mixed|string
     */
    protected function getTableName()
    {
        return (new Post())->getTable();
    }
    
    /**
     * 获取缓存前缀信息
     * @return mixed|string
     */
    protected function getCachePrefix()
    {
        return 'post::';
    }
}
