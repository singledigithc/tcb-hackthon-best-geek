<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2019/7/12
 * Time: 20:11
 */

namespace App\Http\Common\Constant;


class HeaderConst
{
    //用户唯一标识
    const X_UUID = 'x-uuid';

    //用户操作系统
    const X_SYSTEM = 'x-system';

    //用户手机品牌
    const X_BRAND = 'x-brand';

    //用户手机型号
    const X_MODEL = 'x-model';

    //应用程序版本
    const X_APP_VERSION = 'x-app-version';

    //微信版本
    const X_VERSION = 'x-version';

    //应用主体
    const X_APP_SUBJECT = 'x-app-subject';
}