<?php
/**
 * Created by PhpStorm.
 * User: My
 * Date: 2019/7/17
 * Time: 12:57
 */

namespace App\Console\Commands;

use App\Http\Common\Helper\RedisHelper;
use Illuminate\Console\Command;

class ExampleCommand extends Command
{
    protected $signature = 'e {action} {--id=} {--type=} {--time=}';

    protected $description = 'example 测试工具';

    /**
     * MessageCommand constructor.
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * 可调用的action
     * @var array
     */
    private static $actions = [
        "redisSet",
    ];

    /**
     * php artisan e redisSet --id=1
     * $this->argument('action')
     * $this->option('id')
     */
    public function handle() {
        $action = $this->argument('action');
        if (!in_array($action, self::$actions)) {
            echo "命令参数：{action} {--id=} {--type=} {--time=}" . PHP_EOL;
            echo "参数描述： action: 必填：1) redisSet:         缓存测试;" . PHP_EOL;
            exit;
        }
        $this->$action($this->option('id'), $this->option('type'), $this->option('time'));
    }

    public function redisSet($key, $value)
    {
        RedisHelper::setValue($key, $value);
    }
}