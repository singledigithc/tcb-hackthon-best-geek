<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * 其他数据库的服务容器
 * Class OtherServiceProvider
 * @package App\Providers
 */
class OtherServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
