<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //附件信息的注册服务类
        $this->app->singleton("AttachmentRepository", function ($app) {
            return new \App\Http\Common\Repository\AttachmentRepository();
        });

        //题目信息表的注册服务类
        $this->app->singleton("QuestionRepository", function ($app) {
            return new \App\Http\Common\Repository\QuestionRepository();
        });

        //识别信息表的注册服务类
        $this->app->singleton("RecognitionRepository", function ($app) {
            return new \App\Http\Common\Repository\RecognitionRepository();
        });

        //文章表的注册服务类
        $this->app->singleton("PostRepository", function ($app) {
            return new \App\Http\Common\Repository\PostRepository();
        });

        //文章分类表的注册服务类
        $this->app->singleton("PostCategoryRepository", function ($app) {
            return new \App\Http\Common\Repository\PostCategoryRepository();
        });

        //系统配置信息表的注册服务类
        $this->app->singleton("SystemConfigRepository", function ($app) {
            return new \App\Http\Common\Repository\SystemConfigRepository();
        });
    }
}
