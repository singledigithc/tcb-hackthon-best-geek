var WxParse = require('../wxParse/wxParse.js');

Page({
    data: {
        newsTitle:""
    },
    onLoad: function (options) {
        var that = this;

        //加载文章详情
        wx.request({
            url: 'https://recognitionapi.yuanjy.com/v1/post/detail',
            header: {
                'content-type': 'application/json'
            },
            data: {
                postid: options.newsid
            },
            success: function (res) {
                if (res.data.code === 200) {
                    that.setData({
                        newsTitle: res.data.data.title
                    });

                    WxParse.wxParse('article', 'html', res.data.data.content, that, 5);
                }
            }
        });
    },
})