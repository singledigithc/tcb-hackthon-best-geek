let intHandler = null;

Page({
  data: {
    detectResult: "../../assets/images/timg.jpeg",
    detectInterval: 3000,
    detecting: false,
    photoLock: 0,
    alerting: false,
    audioCtx: null,
    cameraCtx: null,
    alertAudio: false
  },
  alertSwitch(e) {
    this.setData({
      alertAudio: e.detail.value
    })
  },
  changeInterval(e) {
    let that = this;
    const detectInterval = e.detail.value;
    if (detectInterval > 10 || detectInterval < 3) {
      return
    }
    that.setData({
      detectInterval: e.detail.value * 1000
    })
  },
  stopDetect(e) {
    this.setData({
      detecting: false,
      alerting: false
    });
    if (intHandler !== null) {
      clearInterval(intHandler);
      intHandler = null;
    }
  },
  startDetect(e) {
    let that = this;
    if (that.data.detecting === true) {
      return
    }
    that.setData({
      detecting: true
    });
    intHandler = setInterval(function () {
      if (that.data.photoLock === 0) {
        that.setData({
          photoLock: 1
        });
        that.data.cameraCtx.takePhoto({
          quality: 'high',
          success: (res) => {
            wx.uploadFile({
              url: 'https://recognitionapi.yuanjy.com/v1/index/mask-detect',
              filePath: res.tempImagePath,
              name: 'image',
              header: {
                "Content-Type": "multipart/form-data",
                "Accept": "application/json"
              },
              success(res) {
                if (res.statusCode === 200) {
                  let data = JSON.parse(res.data);
                  if (data.code === 200) {
                    let noMask = false;
                    for (let i in data.data[0].data) {
                      if (data.data[0].data[i].label === 'NO MASK') {
                        noMask = true;
                      }
                    }
                    if (noMask && !that.data.alerting) {
                      if (that.data.alertAudio) {
                        that.data.myaudio.play();
                      }
                      that.setData({
                        alerting: true
                      })
                    }
                    if (!noMask && that.data.alerting) {
                      that.setData({
                        alerting: false
                      })
                    }

                    that.setData({
                      detectResult: data.data[0].base64,
                    });
                  }
                }
                that.setData({
                  photoLock: 0
                });
              }
            });
          },
        })
      }
    }, that.data.detectInterval)
  },
  onLoad() {
    let myaudio = wx.createInnerAudioContext({});
    myaudio.src = "https://7463-tcb-best-geek-2kr5n-1301287021.tcb.qcloud.la/alert.mp3?sign=44b210847a78383ad6a5229c4d1c9fef&t=1582282166";

    let cameraCtx = wx.createCameraContext();

    this.setData({
      myaudio: myaudio,
      cameraCtx: cameraCtx
    })
  }
});