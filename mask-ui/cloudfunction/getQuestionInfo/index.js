// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

const request = require('request')

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()

  return new Promise((resolve, reject) => {
    request('https://recognitionapi.yuanjy.com/v1/index/question-info', function(error, response, body) {
      if (!error && response.statusCode == 200) {
        resolve(JSON.parse(body))
      } else {
        resolve({"code":-1})
      }
    });
  })
}