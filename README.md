# 罩妖镜小程序

罩妖镜暂时只能检测3M的口罩，使用的时候有两种检测方式，最直接的方式是图片检测。图片检测的流程是先根据自己的口罩类型选择是否带呼吸阀。再拍照或上传图片，编辑截图出呼吸阀上的字样，就可以知道是否通过检测的结果。还有个方式是问卷检测。问卷检测的流程是根据问题和图片提示，选择对应的选项，然后可以得出一个百分比的检测数据，相比图片检测，问卷检测的准确性更高。

## 特性

1. 利用人工智能图像识别和问卷调查分析的技术，帮助用户识别假冒伪劣口罩。
2. 通过发布口罩及病毒防护的新闻资讯，帮助用户更好地进行个人防护。

## 目录说明

+ admin: 管理后台
+ mask-ui: 小程序
+ mask-verifier: 图像识别程序
+ recognitionapi: 后端api接口

## 依赖

- Nginx-1.10.3
- PHP7.2
- MySQL-5.7
- 微信小程序
- Redis-3.2.12

## 部署说明

请参考[部署说明](deployment.md)

## 开发说明

+ 前端项目请参考[微信小程序开发文档](https://developers.weixin.qq.com/miniprogram/dev/framework/)和[Wux Weapp文档](https://www.wuxui.com/#/)。

+ API和管理后台项目请参考[Lumen开发文档](https://lumen.laravel.com/docs/5.5/)和[Yii2开发文档](https://www.yiiframework.com/doc/guide/2.0/zh-cn)。

+ 图像识别程序请参考[PHP-ML开发文档](https://php-ml.readthedocs.io/en/latest/)。

+ 欢迎通过PR的形式贡献代码，如何贡献请参考[项目的贡献指南](contributing.md)。

## Bug 反馈

请通过issue形式反馈bug。

## LICENSE
APACHE-2.0
